import { Component, Input } from '@angular/core';
import { ModalController } from '@ionic/angular'

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage {
  @Input() settings:any;
  constructor(private modalCtrl:ModalController) { }

  async close() {
    await this.modalCtrl.dismiss();
  }

  async updateSettings(){
    await this.modalCtrl.dismiss(this.settings)
  }

}
