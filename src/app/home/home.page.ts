import { Component, OnInit} from '@angular/core';
import { ChuckService } from '../chuck.service'
import { ModalController } from '@ionic/angular'
import { SettingsPage } from '../settings/settings.page'
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit{
  jokes:any = {}
  categories:any
  initialSettings = {
    firstName: 'Keith',
    lastName: 'Brown',
    count: 25
  }
  
  loading:boolean = true;
  constructor(private chuck:ChuckService, private modalCtrl:ModalController, public load: LoadingController) {}

  ngOnInit(){
    // this.getCategoriesList()
    this.getJokesList(this.initialSettings);
  }

  async getJokesList (settings:any){
    this.loading = true;
    await this.chuck.getChuckList(settings).subscribe(data => {
      if (data['value']) { 
        this.jokes = data['value']
        this.loading = false;
      } 
    })
    
  }

  async getCategoriesList(){  //ignoring categories, because most are empty
    await this.chuck.getChuckCategories().subscribe(data => {
      this.categories = data
    })
  }

  async showSettings(){
    const modal = await this.modalCtrl.create({
      component: SettingsPage,
      componentProps: {
        settings: this.initialSettings
      }
    });
    await modal.present(); 
    await modal.onDidDismiss().then(res => {
      if(res.data){
        this.initialSettings = res.data;
        this.getJokesList(res.data);
        //not using local data - would just save to a realtime database... (ie: Firebase)
      }
    })
  }

  

}
