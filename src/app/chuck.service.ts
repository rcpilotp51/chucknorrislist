import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ChuckService {
  url = 'http://api.icndb.com/jokes/random/'
  url2 = 'http://api.icndb.com/categories'
  

  constructor(private http: HttpClient) { }
  getChuckList(settings:any){
    return this.http.get(`${this.url}${settings.count}?escape=javascript&firstName=${settings.firstName}&lastName=${settings.lastName}`);
  }
  getChuckCategories(){
    return this.http.get(`${this.url2}`);
  }
  
}
