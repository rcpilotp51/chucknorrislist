# chuckNorrisList

### Prerequisites
- Download and install [NodeJS](https://nodejs.org/en/download/)
- run `npm install -g ionic`
- clone this repository
- cd to the "chuckNorrisList" directory or the one you created
- run `npm i`
- run `ionic serve`
- enjoy 

### Usage
 - Using the settings icon you can change the name returned from `Keith Brown` to your own. The default API returns `Chuck Norris` jokes, but it is fun to substitute your own name.
 - You may also choose the number of results to display.
 
### Application (API)
 - This app uses [The Internet Chuck Norris Database](http://www.icndb.com/api/) and provides the persona and number of results you specify. 

#### There are several things that I would have done a bit differently if I approached this app with the intention of going to production
- I would have mapped the result to a custom `Joke` class but since there were only a few nodes in the data returned, it seemed unnecessary.
- I originally was going to allow the user to choose the category of the joke in the settings modal, but most of the results for the `categories` array came back empty. (Not as advertised in the documentation)
- I would have given it a bit more style, overall.
- I would have implemented "lazy loading" of the line items, but the data set returned was extremely small and the app wont be configured for mobile; where a loading component is mostly needed.

### Notes
- All componets, services and pages can easily be ported into a [VueJS](https://vuejs.org/) application.